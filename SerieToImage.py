from PIL import Image
import random
import csv
import math
import numpy as np
import cPickle
import Helper as helper
import pandas as pd
import pycuda.driver as cuda


def serieAImagen(serieOriginal, altoImagen, anchoImagen):

    serie = helper.normalizarSerieParaImagen(serieOriginal)

    lenSerie = len(serie)

    tamanioLadoImagen = int(math.sqrt(lenSerie))

    areaImagen = int(math.pow(tamanioLadoImagen, 2))

    serie = serie[:areaImagen]

    imageArray = []

    for value in serie:
        imageArray.append(int(value))

    datosImagen = np.array(imageArray).reshape(tamanioLadoImagen, tamanioLadoImagen)

    return datosImagen

def serieAImagen2(serieOriginal):

    imageArray = []

    for valor1 in serieOriginal:
        for valor2 in serieOriginal:

            ac = math.cos(valor1 + valor2)

            imageArray.append(ac)

    imageArray = helper.normalizarSerieParaImagen(imageArray)

    datosImagen = np.array(imageArray).reshape(len(serieOriginal), len(serieOriginal))

    return datosImagen


def obtenerBancoDeImagenes2(tamanioImagen, serieTiempo, paso):

    arrayInputTarget = []

    for i in range (0, (len(serieTiempo) - tamanioImagen - paso) + 1 ):

        input = serieTiempo[ i: i + tamanioImagen]

        input = helper.normalizarSerieParaImagen2(input)

        target = serieTiempo[ i + tamanioImagen + paso - 1]

        input = map(lambda x : math.acos(x), input)

        datosImagen = serieAImagen2(input)

        target = np.array(target)

        arrayInputTarget.append({'imagen' : datosImagen, 'target' : target})

    return arrayInputTarget


def obtenerBancoDeImagenesGAFCUDA(tamanioImagen, serieTiempo, paso):

    obtenerBancoDeImagenesGAFCUDA = helper.obtenerFuncionKernel("obtenerBancoDeImagenesGAFCUDA")

    n_elem = tamanioImagen
    block_size = 32
    n_blocks = (n_elem + block_size - 1) / block_size;
    block_conf = (block_size, 1, 1)
    grid_conf = (n_blocks, 1)

    lenSerieTiempo = len(serieTiempo)

    obtenerBancoDeImagenesGAFCUDA(cuda.InOut(serieTiempo), np.float32(lenSerieTiempo), np.float32(tamanioImagen), np.float32(paso), block = block_conf, grid = grid_conf)

    '''
    arrayInputTarget = []

    for i in range (0, (len(serieTiempo) - tamanioImagen - paso) + 1 ):

        input = serieTiempo[ i: i + tamanioImagen]

        min_value = min(input)
        max_value = max(input)

        input = np.asarray(input, dtype = np.float32)

        normalizarEntreMenusUnoYUno(np.int32(n_elem), np.float32(min_value), np.float32(max_value), cuda.InOut(input), block = block_conf, grid = grid_conf)

        target = serieTiempo[ i + tamanioImagen + paso - 1]

        input = map(lambda x : math.acos(x), input)

        datosImagen = serieAImagen2(input)

        target = np.array(target)

        arrayInputTarget.append({'imagen' : datosImagen, 'target' : target})
    '''

    return arrayInputTarget


def obtenerBancoDeImagenes(altoImagen, anchoImagen, serieTiempo, paso):

    areaImagen = altoImagen * anchoImagen

    arrayInputTarget = []

    for i in range (0, len(serieTiempo) - areaImagen ):

        input = serieTiempo[ i: i + areaImagen]
        target = serieTiempo[ i + areaImagen]

        datosImagen = serieAImagen(input, altoImagen, anchoImagen)

        arrayInputTarget.append({'imagen' : datosImagen, 'target' : target})

    return arrayInputTarget

def datosAImagen(bancoDeImagenes, nombreBancoImagenes, directorioBancoImagenes):

    numeroImagen = 0

    relaciones = []
    for datos in bancoDeImagenes:

        numeroImagen+=1

        datosImagen = datos['imagen']
        datosTarget = datos['target']

        altoImagen = datosImagen.shape[0]
        anchoImagen = datosImagen.shape[1]

        imagen = Image.new("L", (altoImagen, anchoImagen))
        imagen.putdata(datosImagen.flatten())

        nombreImagen = str(numeroImagen) + '.jpeg'

        #imagen.save(directorioBancoImagenes+"/"+nombreImagen,"JPEG")

        relaciones.append([nombreImagen, datosTarget])

    relaciones = np.array(relaciones)

    df = pd.DataFrame(relaciones)
    df.to_csv(directorioBancoImagenes+"/"+nombreBancoImagenes+".csv")

def mostrarImagen(datosImagen):

    altoImagen = datosImagen.shape[0]
    anchoImagen = datosImagen.shape[1]

    imagen = Image.new("L", (altoImagen, anchoImagen))
    imagen.putdata(datosImagen.flatten())
    imagen.show()

def rotarImagen(datosImagen, grados):

    altoImagen = datosImagen.shape[0]
    anchoImagen = datosImagen.shape[1]

    imagen = Image.new("L", (altoImagen, anchoImagen))
    imagen.putdata(datosImagen.flatten())

    imagen = imagen.rotate(grados)

    return np.asarray(imagen)

if __name__ == "__main__":

    nombreArchivo = "wind_el_fresno_serie_3000_tallerML"
    rutaArchivo = nombreArchivo+'.csv'
    serieOriginal = helper.leerArchivo(rutaArchivo)
    serieOriginal = helper.normalizarSerie(serieOriginal)

    altoImagen = 44
    anchoImagen = 44

    bancoImagenes = obtenerBancoDeImagenes2(anchoImagen, serieOriginal)

    print len(bancoImagenes)

    nombreDataSetImagenes = "dataset"+str(altoImagen)+"x"+str(anchoImagen)+"_"+str(len(serieOriginal))+".data"

    datosAImagen(bancoImagenes, nombreDataSetImagenes)


    '''
    cPickle.dump(arrayInputTarget, open(nombreDataSetImagenes,"wb"))

    '''
