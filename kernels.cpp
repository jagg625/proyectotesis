__global__ void normalize(int iterations, double min_value, double max_value, double invalid_value, double *data) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	for(int i = index; i < iterations; i += blockDim.x * gridDim.x) { //Grid-Stride Loop
		if(data[index] != invalid_value)
			data[index] = (data[index] - min_value) / (max_value - min_value);
	}
}


__global__ void obtenerBancoDeImagenesGAFCUDA(float *datosSerie, int logitudDatosSerie, int tamanioImagen, int paso){

	int index = blockIdx.x * blockDim.x + threadIdx.x;

	for (int i = index; i < (logitudDatosSerie - tamanioImagen - paso) + 1 ; i += blockDim.x * gridDim.x){
		
		float datosSerie [tamanioImagen];

		for (int j = i; j < tamanioImagen; j++){
			datosSerie[]
		}
	}

	arrayInputTarget = []

    for i in range (0, (len(serieTiempo) - tamanioImagen - paso) + 1 ):

        input = serieTiempo[ i: i + tamanioImagen]

        min_value = min(input)
        max_value = max(input)

        input = np.asarray(input, dtype = np.float32)

        normalizarEntreMenusUnoYUno(np.int32(n_elem), np.float32(min_value), np.float32(max_value), cuda.InOut(input), block = block_conf, grid = grid_conf)

        target = serieTiempo[ i + tamanioImagen + paso - 1]

        input = map(lambda x : math.acos(x), input)

        datosImagen = serieAImagen2(input)

        target = np.array(target)

        arrayInputTarget.append({'imagen' : datosImagen, 'target' : target})

    return arrayInputTarget

}

__global__ void normalizarEntreMenusUnoYUno(int iterations, float min_value, float max_value, float *data) {


	min_value = min(data)
	max_value = max(data)


	int thread = blockIdx.x * blockDim.x + threadIdx.x;
	
	for(int index = thread; index < iterations; index += blockDim.x * gridDim.x) { //Grid-Stride Loop

		data[index] = ((data[index] - max_value) + (data[index] - min_value))/ (max_value - min_value);
	}
}

__device__ float min(float *data, int length) {
	float min_value = data[0];

	for(int i = 1; i < length; i++)
		if(data[i] < min_value)
			min_value = data[i];

	return min_value;
}

__device__ float max(float *data, int length){

	float max_value = data[0];
	
	for(int i = 1; i < length; i++)
		if(data[i] < max_value)
			max_value = data[i];

	return max_value;

}

	



