
import cPickle
import numpy as np
np.set_printoptions(threshold=np.inf)
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Activation
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.utils import np_utils
from keras import backend as K
K.set_image_dim_ordering('th')
from keras.models import load_model
import csv
import SerieToImage
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
import Helper as helper
import time
from pycuda.compiler import DynamicSourceModule
import pycuda.driver as cuda
import pycuda.autoinit

# fix random seed for reproducibility
seed = 7
np.random.seed(seed)

def leerArchivo(ruta):
    serie = []

    with open(ruta, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            dato = float(row[0])
            serie.append(dato)
    return serie

def guardarMSEContinuoExcel(dirArchivoCSV, tag1, valor1, tagValor1, valor2, tagValor2):

    with open(dirArchivoCSV, "a") as ArchivoNuevoCSV:

        writer = csv.DictWriter(ArchivoNuevoCSV, fieldnames= ['iteracion', tagValor1, tagValor2])

        for i in range(0, len(valor1)):
            valor1 = str(valor1[i])
            valor2 = str(valor2[i])

            valor1 = valor1.replace("[", "")
            valor1 = valor1.replace("]", "")

            valor2 = valor2.replace("[", "")
            valor2 = valor2.replace("]", "")

            writer.writerow({'iteracion': tag1, tagValor1: valor1, tagValor2: valor2 })

def guardarExcel(archivoCSV, tar, out, nb_columna1, nb_columna2):

    with open(archivoCSV, "wb") as ArchivoNuevoCSV:

        writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

        writer.writerow([str(nb_columna1), str(nb_columna2)])

        for i in range(0, len(tar)):
            tarTemp = str(tar[i])
            outTemp = str(out[i])
            aux1 = tarTemp.replace("[", "")
            tarAdd = aux1.replace("]", "")

            aux2 = outTemp.replace("[", "")
            outAdd = aux2.replace("]", "")

            writer.writerow( (tarAdd, outAdd) )

def obtenerDataSet(datos):

    #datos = cPickle.load(open(dataSetNombre,"rb"))

    datosImagenes = []
    datosTarget = []

    for value in datos:
        datoImagen = value['imagen']
        datoTarget = value['target']

        datosImagenes.append(datoImagen)
        datosTarget.append(datoTarget)

    return datosImagenes, datosTarget

def fitModel(datosImagenesEntrenamiento, datosTargetEntrenamiento, tamanioImagen, epocas, valorDropout):

    # build the model
    model = baseline_model(tamanioImagen, valorDropout)

    # Fit the model
    model.fit(datosImagenesEntrenamiento, datosTargetEntrenamiento, epochs=epocas, verbose=2)

    return model

def baseline_model(tamanioImagen, valorDropout):
    # create model
    model = Sequential()
    model.add(Conv2D(64, (5, 5), input_shape=(1, tamanioImagen, tamanioImagen), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (5, 5), input_shape=(1, tamanioImagen, tamanioImagen), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    # Dropout va de 0 - 1
    model.add(Dropout(valorDropout))
    model.add(Flatten())

    model.add(Dense(32))
    model.add(Dropout(valorDropout))
    model.add(Activation('relu'))

    model.add(Dense(32))
    model.add(Dropout(valorDropout))
    model.add(Activation('relu'))

    model.add(Dense(1))
    model.add(Activation('linear'))

    # Compile model
    model.compile(loss='mean_squared_error', optimizer='adam')

    return model

def cross_validation():

    # Hora y fecha inicio
    horaInicio = time.strftime("%H-%M-%S")
    fechaInicio = time.strftime("%d-%m-%Y")

    nombreArchivo = "wind_el_fresno_serie_3000_tallerML"
    rutaArchivo = nombreArchivo+'.csv'
    serieOriginal = leerArchivo(rutaArchivo)
    serieOriginal = helper.normalizarSerie(serieOriginal)

    tamanioSerieTiempo = 8000

    serie = serieOriginal[:tamanioSerieTiempo]

    tamanioImagen = 24

    tipoSerieAImagen = 2

    if tipoSerieAImagen == 1:
        bancoImagenes = SerieToImage.obtenerBancoDeImagenes(tamanioImagen, tamanioImagen, serie)
    else:
        bancoImagenes = SerieToImage.obtenerBancoDeImagenes2(tamanioImagen, serie)

    datosImagenes, datosTarget = obtenerDataSet(bancoImagenes)

    datosImagenes = np.array(datosImagenes)
    datosTarget = np.array(datosTarget)

    data_train, data_test, target_train, target_test = train_test_split(datosImagenes, datosTarget, test_size=0.33, random_state=0)
    epocas = 1000

    kf = KFold(n_splits = 5)

    for valorDropout in np.arange(0.2, 0.8, 0.05):

        mseTotal = 0

        nCrossValidationFold = 0
        for X_train_index, X_test_index in kf.split(data_train):

            X_train = data_train[X_train_index]
            X_test = data_train[X_test_index]

            y_train = target_train[X_train_index]
            y_test = target_train[X_test_index]

            # reshape to be [samples][pixels][width][height]
            X_train = X_train.reshape(X_train.shape[0], 1, tamanioImagen, tamanioImagen).astype('float32')
            X_test = X_test.reshape(X_test.shape[0], 1, tamanioImagen, tamanioImagen).astype('float32')

            # normalize inputs from 0-255 to 0-1
            X_train = X_train / 255
            X_test = X_test / 255

            y_train = y_train.reshape(len(y_train), 1)
            y_test = y_test.reshape(len(y_test), 1)

            model = fitModel(X_train, y_train, tamanioImagen, epocas, valorDropout)

            #nombreModelo = 'model_'+str(tamanioImagen)+'x'+str(tamanioImagen)+'_'+str(tamanioSerieTiempo)+'_epocas-'+str(epocas)+'.h5'

            #model.save(nombreModelo)

            #model = load_model(nombreModelo)

            predicted = model.predict(X_test)

            nombreResultado = 'resultados_'+fechaInicio+"_"+horaInicio+"_"+str(tamanioImagen)+"x"+str(tamanioImagen)+"_"+str(tamanioSerieTiempo)+'_epocas_'+str(epocas)+"_dropout_"+str(valorDropout)+"_fold_"+str(nCrossValidationFold)+'.csv'

            guardarExcel(nombreResultado, y_test, predicted, 'tar', 'out')

            mseTest = helper.MSE(y_test, predicted)

            mseTotal+= mseTest

            #mseTrain = MSE(y_train, predicted)

            nCrossValidationFold += 1

        nombreIteracion = str(tamanioImagen)+"x"+str(tamanioImagen)+"_"+str(tamanioSerieTiempo)+'_epocas_'+str(epocas)

        msePromedio = mseTotal / kf.get_n_splits(data_train)

        guardarMSEContinuoExcel("resultadosMSE"+fechaInicio+"_"+horaInicio+".csv", nombreIteracion, [mseTotal], 'MSE_TOTAL', [valorDropout], 'DROP_OUT')

        #print "MSETOTAL :", mseTotal, " _Dropout : ",valorDropout


    # Hora y fecha inicio
    horaFin = time.strftime("%H:%M:%S")
    fechaFin = time.strftime("%d/%m/%Y")

    print "Inicio: ", fechaInicio," ", horaInicio, " terminio: ",fechaFin, " ", horaFin

def rotar(datosImagenesTrain, datosImagenesTrainTarget, grados):

    nuevoDatosImagenesTrain = []
    nuevoDatosImagenesTrainTarget = []

    for datosImagenTrainIndex in range(len(datosImagenesTrain)):

        datosImagenTrain = datosImagenesTrain[datosImagenTrainIndex]

        datosImagenTrainRotada = SerieToImage.rotarImagen(datosImagenTrain, grados)

        nuevoDatosImagenesTrain.append(datosImagenTrain)
        nuevoDatosImagenesTrain.append(datosImagenTrainRotada)
        
        target = datosImagenesTrainTarget[datosImagenTrainIndex]

        nuevoDatosImagenesTrainTarget.append(target)
        nuevoDatosImagenesTrainTarget.append(target)

    return np.array(nuevoDatosImagenesTrain), np.array(nuevoDatosImagenesTrainTarget)

def simulacion():

    nombreArchivo = "sector19"
    rutaArchivo = nombreArchivo+'.csv'
    serieOriginal = leerArchivo(rutaArchivo)

    tamanioSerieTiempo = 8000

    serie = serieOriginal[:tamanioSerieTiempo]

    # Datos de entrenamiento = 80 % de la serie original
    data_train, data_test = train_test_split(serie, test_size=0.20, shuffle=False)

    # Mejor tamanio de imagen encontrado en experimiento de tamanios de imagenes
    tamanioImagen = 24
    numeroPaso = 1

    tipoSerieAImagen = 'GRAMIAN_ANGULAR_FIELD'

    # obtener el banco de imagenes para pruebas
    if tipoSerieAImagen == 'ESCANEO_LINEAL':
        bancoImagenesTest = SerieToImage.obtenerBancoDeImagenes(tamanioImagen, tamanioImagen, data_test)
    elif tipoSerieAImagen == 'GRAMIAN_ANGULAR_FIELD':
        bancoImagenesTest = SerieToImage.obtenerBancoDeImagenes2(tamanioImagen, data_test, numeroPaso)

    datosImagenesTest, datosImagenesTestTarget = obtenerDataSet(bancoImagenesTest)

    datosImagenesTest = np.array(datosImagenesTest)
    datosImagenesTestTarget = np.array(datosImagenesTestTarget)

    nombreModelo = 'Exprimiento4AguaSector19SinSuavizar_resultados_dataset07-12-2017_13-37-45_24x24_1080_epocas73_dropout0.4_tipoSerieAImagenGRAMIAN_ANGULAR_FIELD_paso:1.h5'

    model = load_model(nombreModelo)

    X_test = datosImagenesTest

    y_test = datosImagenesTestTarget

    # reshape to be [samples][pixels][width][height]
    X_test = X_test.reshape(X_test.shape[0], 1, tamanioImagen, tamanioImagen)

    # normalize inputs from 0-255 to 0-1
    X_test = X_test / 255

    y_test = y_test.reshape(len(y_test), 1)

    prediccionEnTest = model.predict(X_test)

    mseTest = helper.MSE(y_test, prediccionEnTest)

    print mseTest

def experimento():

    idExperimento = 'Exprimiento4AguaSector19SinSuavizar'

    # Hora y fecha inicio
    horaInicio = time.strftime("%H-%M-%S")
    fechaInicio = time.strftime("%d-%m-%Y")

    nombreArchivo = "sector19"
    rutaArchivo = nombreArchivo+'.csv'
    serieOriginal = leerArchivo(rutaArchivo)

    tamanioSerieTiempo = 8000

    serie = serieOriginal[:tamanioSerieTiempo]

    #serie = helper.normalizarSerie(serie)

    # Datos de entrenamiento = 80 % de la serie original
    data_train, data_test = train_test_split(serie, test_size=0.20, shuffle=False)
 
    # MEDIA_MOVIBLE MEDIANA_MOVIBLE
    tipoSuavizado = 'MEDIANA_MOVIBLE_X'

    # Datos de entrenamiento suavizados
    if tipoSuavizado == 'MEDIA_MOVIBLE' :
        data_train = helper.suavizarSeriePromedioMovible(data_train)
    elif tipoSuavizado == 'MEDIANA_MOVIBLE':
        data_train = helper.running_median_insort(data_train, 3)

    # Mejor tamanio de imagen encontrado en experimiento de tamanios de imagenes
    tamanioImagen = 24

    tiposSerieImagen = ['GRAMIAN_ANGULAR_FIELD']

    for numeroPaso in range(22,25,1):

        #Los tipos de imagenes son ESCANEO_LINEAL y GRAMIAN_ANGULAR_FIELD
        for tipoSerieAImagen in tiposSerieImagen:

            # obtener el banco de imagenes para entrenamiento
            if tipoSerieAImagen == 'ESCANEO_LINEAL':
                bancoImagenesTrain = SerieToImage.obtenerBancoDeImagenes(tamanioImagen, tamanioImagen, data_train)
            elif tipoSerieAImagen == 'GRAMIAN_ANGULAR_FIELD':
                bancoImagenesTrain = SerieToImage.obtenerBancoDeImagenes2(tamanioImagen, data_train, numeroPaso)

            datosImagenesTrain, datosImagenesTrainTarget = obtenerDataSet(bancoImagenesTrain)

            datosImagenesTrain = np.array(datosImagenesTrain)
            datosImagenesTrainTarget = np.array(datosImagenesTrainTarget)

            #datosImagenesTrain, datosImagenesTrainTarget = rotar(datosImagenesTrain, datosImagenesTrainTarget, 90)

            # obtener el banco de imagenes para pruebas
            if tipoSerieAImagen == 'ESCANEO_LINEAL':
                bancoImagenesTest = SerieToImage.obtenerBancoDeImagenes(tamanioImagen, tamanioImagen, data_test)
            elif tipoSerieAImagen == 'GRAMIAN_ANGULAR_FIELD':
                bancoImagenesTest = SerieToImage.obtenerBancoDeImagenes2(tamanioImagen, data_test, numeroPaso)

            datosImagenesTest, datosImagenesTestTarget = obtenerDataSet(bancoImagenesTest)

            datosImagenesTest = np.array(datosImagenesTest)
            datosImagenesTestTarget = np.array(datosImagenesTestTarget)

            #SerieToImage.mostrarImagen(datosImagenesTest[0])

            # Mejor dropout encontrado en experimiento de dropout
            valorDropout = 0.4

            X_train = datosImagenesTrain
            X_test = datosImagenesTest

            y_train = datosImagenesTrainTarget
            y_test = datosImagenesTestTarget

            # reshape to be [samples][pixels][width][height]
            X_train = X_train.reshape(X_train.shape[0], 1, tamanioImagen, tamanioImagen)
            X_test = X_test.reshape(X_test.shape[0], 1, tamanioImagen, tamanioImagen)

            # normalize inputs from 0-255 to 0-1
            X_train = X_train / 255
            X_test = X_test / 255

            y_train = y_train.reshape(len(y_train), 1)
            y_test = y_test.reshape(len(y_test), 1)

            mejorMSETest = 9999999999
            mejorPrediccionTest = None
            mejorPrediccionTestNombre = ''
            mejorModelo = None

            for epocas in range(50, 1000, 5):

                model = fitModel(X_train, y_train, tamanioImagen, epocas, valorDropout)

                prediccionEnTest = model.predict(X_test)

                prediccionEnTrain = model.predict(X_train)

                mseTest = helper.MSE(y_test, prediccionEnTest)

                mseTrain = helper.MSE(y_train, prediccionEnTrain)

                if mseTest < mejorMSETest:

                    mejorPrediccionTestNombre = idExperimento+'_resultados_'+"dataset"+fechaInicio+"_"+horaInicio+"_"+str(tamanioImagen)+"x"+str(tamanioImagen)+"_"+str(len(serie))+"_epocas"+str(epocas)+"_dropout"+str(valorDropout)+"_tipoSerieAImagen"+tipoSerieAImagen+"_paso:"+str(numeroPaso)

                    mejorMSETest = mseTest 

                    mejorPrediccionTest = prediccionEnTest

                    mejorModelo = model

                nombreIteracion = idExperimento+"_"+fechaInicio+"_"+horaInicio+"_"+str(tamanioImagen)+"x"+str(tamanioImagen)+"_"+str(tamanioSerieTiempo)+'_epocas:'+str(epocas)+"_dropout"+str(valorDropout)+"_tipoSerieAImagen"+tipoSerieAImagen+"_paso:"+str(numeroPaso)

                guardarMSEContinuoExcel(idExperimento+"_resultadosMSE"+fechaInicio+"_"+horaInicio+".csv", nombreIteracion, [mseTest], 'MSE_TEST', [mseTrain], 'MSE_TRAIN')

            nombreModelo = mejorPrediccionTestNombre+'.h5'

            mejorModelo.save(nombreModelo)

            guardarExcel(mejorPrediccionTestNombre+'.csv', y_test, mejorPrediccionTest, 'tar', 'out')

    #print "MSETOTAL :", mseTotal, " _Dropout : ",valorDropout

    # Hora y fecha inicio
    horaFin = time.strftime("%H:%M:%S")
    fechaFin = time.strftime("%d/%m/%Y")

    print "Inicio: ", fechaInicio," ", horaInicio, " terminio: ",fechaFin, " ", horaFin
    

if __name__ == "__main__":
    import cProfile,pstats
    cProfile.run('experimento()','.prof')
    prof = pstats.Stats('.prof')
    prof.strip_dirs().sort_stats('time').print_stats(30)
