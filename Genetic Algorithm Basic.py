"""
Visualize Genetic Algorithm to find a maximum point in a function.

Visit my tutorial website for more: https://morvanzhou.github.io/tutorials/
"""
import numpy as np

DNA_SIZE = 18            # DNA length
POP_SIZE = 100           # population size
CROSS_RATE = 0.8         # mating probability (DNA crossover)
MUTATION_RATE = 0.003    # mutation probability
N_GENERATIONS = 100

def F(x): 

    suma = 0

    #epocas 20, 30, 40, 50, 60, 70, 80, 90
    #maximo binario 111 = 3
    #numEpochs = 3 posiciones 
    numEpochs = x[:3]
    numEpochs = binarioADecimal(numEpochs)

    # 0 no es valido
    #lr 0.0001, 0.0006, 0.0011, 0.0016, 0.0020, 0.0025, 0.0030
    #maximo binario 111 = 3
    #learningRate = 3 posiciones

    learningRate = x[3:6]
    learningRate = binarioADecimal(learningRate)

    # 0 no es valido
    #training rate 70, 80, 90 porciento
    #maximo binario 11 = 3
    #trainingRate = 2 posiciones

    trainingRate = x[6:8]
    trainingRate = binarioADecimal(trainingRate)

    # 0 no es valido
    #training GradientDescentOptimizer, AdamOptimizer and RMSPropOptimizero
    #maximo binario 11 = 3
    #optimizer = 2 posiciones

    optimizer = x[8:10]
    optimizer = binarioADecimal(optimizer)

    # 0 no es valido
    # 3 no es valido 
    #activation relu elu 
    #maximo binario 11 = 3
    #activation = 2 posiciones  
    activation = x[10:12]
    activation = binarioADecimal(activation)

    # 0 no es valido
    #filterSizes 3,4,5
    #maximo binario 11 = 3
    #filterSize = 2 posiciones 
    filterSize = x[12:14]
    filterSize = binarioADecimal(filterSize)

    # 0 no es valido
    #filterSizes 1,2,3
    #maximo binario 11 = 3
    #strides = 2 posiciones  
    strides = x[14:16]
    strides = binarioADecimal(strides)

    #padding 1,2,3
    #maximo binario 1 = 1
    #padding  = 1 posicion
    padding = x[16:17]
    padding = binarioADecimal(padding)

    #pooling max_pooling, avg_pooling
    #maximo binario 1 = 1
    #pool = 1
    pool = x[17:18]
    pool = binarioADecimal(pool)

    return suma   # to find the maximum of this function

def binarioADecimal(binario):

    binarioStr = "".join(str(x) for x in binario)

    return int(binarioStr, 2)

def get_fitness(pred): 
    return pred + 1e-3 - np.min(pred)

def select(pop, fitness):    # nature selection wrt pop's fitness
    idx = np.random.choice(np.arange(POP_SIZE), size=POP_SIZE, replace=True,
                           p=fitness/fitness.sum())
    return pop[idx]

def crossover(parent, pop):     # mating process (genes crossover)
    if np.random.rand() < CROSS_RATE:
        i_ = np.random.randint(0, POP_SIZE, size=1)                             # select another individual from pop
        cross_points = np.random.randint(0, 2, size=DNA_SIZE).astype(np.bool)   # choose crossover points
        parent[cross_points] = pop[i_, cross_points]                            # mating and produce one child
    return parent


def mutate(child):
    for point in range(DNA_SIZE):
        if np.random.rand() < MUTATION_RATE:
            child[point] = 1 if child[point] == 0 else 0
    return child


pop = np.random.randint(2, size=(POP_SIZE, DNA_SIZE))   # initialize the pop DNA


for x in range(N_GENERATIONS):

    F_values =  np.array(map(lambda x: F(x), pop))

    # GA part (evolution)
    fitness = get_fitness(F_values)
    print("Most fitted DNA: ", pop[np.argmax(fitness), :])
    pop = select(pop, fitness)
    pop_copy = pop.copy()
    for parent in pop:
        child = crossover(parent, pop_copy)
        child = mutate(child)
        parent[:] = child       # parent is replaced by its child
