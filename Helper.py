import csv
from collections import deque
from bisect import insort, bisect_left
from itertools import islice
from pycuda.compiler import DynamicSourceModule
import pycuda.driver as cuda
import pycuda.autoinit

def leerArchivo(ruta):
    serie = []

    with open(ruta, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            dato = float(row[0])
            serie.append(dato)
    return serie

def normalizarSerie(serie):
    minimo = min(serie)
    maximo = max(serie)

    return map(lambda x : (x - minimo) / (maximo - minimo), serie )

def normalizarSerieParaImagen2(serie):
        minimo = min(serie)
        maximo = max(serie)

        return map(lambda x : ((x - maximo) +  (x - minimo)) / (maximo - minimo), serie )

def normalizarSerieParaImagen(serie):
    minimo = min(serie)
    maximo = max(serie)

    return map(lambda x : (x - minimo) * 255 / (maximo - minimo), serie )

def suavizarSeriePromedioMovible(serie):

    serieSuavizada = []

    MovingAverageSize = 3
    MovingAverageSizeAux = MovingAverageSize

    for index in range(len(serie)):

        datos = serie[index:MovingAverageSizeAux]

        if len(datos) < MovingAverageSize:
            break

        serieSuavizada.append(sum(datos) / MovingAverageSize )

        MovingAverageSizeAux += 1

    return serieSuavizada

def running_median_insort(seq, window_size):
	"""Contributed by Peter Otten"""
	seq = iter(seq)
	d = deque()
	s = []
	result = []
	for item in islice(seq, window_size):
		d.append(item)
		insort(s, item)
		result.append(s[len(d)//2])
	m = window_size // 2

	for item in seq:
		old = d.popleft()
		d.append(item)
		del s[bisect_left(s, old)]
		insort(s, item)
		result.append(s[m])
        
	return result

def MSE(originalList, forecastingList):
    sum = 0
    for index, elemet in enumerate(originalList):
        forecasting = forecastingList[index]
        sum = sum + ((forecasting - elemet)**2)

    return sum / len(originalList)

def obtenerFuncionKernel(funcion):
    kernels = DynamicSourceModule(open("kernels.cpp", "r").read(), cuda_libdir = '/usr/local/cuda/lib64') #kernels.cpp = nombre de archivo externo
    cuda_function = kernels.get_function(funcion) #Selecciona el kernel a utilizar

    return cuda_function


